﻿using OMS.NET.Controllers;
using OMS.NET.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OMS.NET.Services
{
    internal class ModifyFulfillmentService : IModifyFulfillmentService
    {
        private OMSContext db = new OMSContext();
        public virtual IPaymentService paymentService { get ; set; }
        public virtual IEmailService emailService { get; set; }
        public virtual IShippingService shippingService { get; set; }

        public SalesOrder ModifyToShipping(string lineItemId, SalesOrder salesOrder)
        {
            if (salesOrder != null && !string.IsNullOrEmpty(salesOrder.CustomerOrderId))
            {
                SalesOrder originalSalesOrder = db.sales.Find(salesOrder.CustomerOrderId);
                OrderLine orderLine = FetchOrderLineFromItemId(lineItemId, originalSalesOrder);
                double shippingAmount = GetShippingAmount(orderLine.CustomerSKU);
                if (orderLine != null && orderLine.LineCharges != null && orderLine.LineCharges.TotalCharges != 0)
                {
                    if (orderLine.LineCharges.TotalCharges > 0)
                    {
                        AuthorizationRequestDto authorizationRequestDto = new AuthorizationRequestDto("VISA", "23445567", "12/24", "123", shippingAmount);
                        AuthorizationResponseDto responseDto = paymentService.Authorize(authorizationRequestDto);
                        if (responseDto != null && !string.IsNullOrEmpty(responseDto.Id) && !double.IsNaN(responseDto.Amount))
                        {
                            originalSalesOrder.PaymentInfo.AuthorizedAmount = (originalSalesOrder.PaymentInfo.AuthorizedAmount + responseDto.Amount);
                            db.sales.Add(originalSalesOrder);
                            emailService.SendEmail(buildEmailRequest(originalSalesOrder));
                            PropertyCopier<SalesOrder, SalesOrder>.Copy(originalSalesOrder, salesOrder);
                        }
                    }
                }
            }
            return salesOrder;
        }

        private OrderLine FetchOrderLineFromItemId(string lineItemId, SalesOrder salesOrder)
        {
            List<OrderLine> orderLines = new List<OrderLine>(db.orders);
            OrderLine orderLine = (from line in orderLines where line.LineItemId == lineItemId select line).FirstOrDefault();
            return orderLine;
        }

        private double GetShippingAmount(String skuId)
        {
            Shipping shipping = shippingService.FetchShippingCharges(skuId);
            if (shipping != null)
            {
                return shipping.StandardShipping;
            }

            return 0;
        }

        public EmailRequestDto buildEmailRequest(SalesOrder salesOrder)
        {
            return new EmailRequestDto("1234", "Modify fulfillment", "Your line item have been successfully modified for fulfillment", "Modify to shipping from store pickup");
        }

        public SalesOrder ModifyToStorePickup(string lineItemId, SalesOrder salesOrder)
        {
            if (salesOrder != null && !string.IsNullOrEmpty(salesOrder.CustomerOrderId))
            {
                SalesOrder originalSalesOrder = db.sales.Find(salesOrder.CustomerOrderId);
                OrderLine orderLine = FetchOrderLineFromItemId(lineItemId, originalSalesOrder);
                double shippingAmount = GetShippingAmount(orderLine.CustomerSKU);
                if (orderLine != null && orderLine.LineCharges != null && !double.IsNaN(orderLine.LineCharges.TotalCharges))
                {
                    if (orderLine.LineCharges.TotalCharges > 0)
                    {
                        AuthorizationRequestDto authorizationRequestDto = new AuthorizationRequestDto("VISA", "23445567", "12/24", "123", shippingAmount);
                        AuthorizationResponseDto responseDto = paymentService.ReverseAuth(authorizationRequestDto);
                        if (responseDto != null && !string.IsNullOrEmpty(responseDto.Id) && !double.IsNaN(responseDto.Amount))
                        {
                            originalSalesOrder.PaymentInfo.AuthorizedAmount = (originalSalesOrder.PaymentInfo.AuthorizedAmount - responseDto.Amount);
                            db.sales.Add(originalSalesOrder);
                            db.SaveChanges();
                            emailService.SendEmail(buildEmailRequest(originalSalesOrder));
                            PropertyCopier<SalesOrder, SalesOrder>.Copy(originalSalesOrder, salesOrder);
                        }
                    }
                }
            }

            return salesOrder;
        }
    }

    internal class PropertyCopier<TParent, TChild> where TParent : class
                                            where TChild : class
    {
        public static void Copy(TParent parent, TChild child)
        {
            var parentProperties = parent.GetType().GetProperties();
            var childProperties = child.GetType().GetProperties();

            foreach (var parentProperty in parentProperties)
            {
                foreach (var childProperty in childProperties)
                {
                    if (parentProperty.Name == childProperty.Name && parentProperty.PropertyType == childProperty.PropertyType)
                    {
                        childProperty.SetValue(child, parentProperty.GetValue(parent));
                        break;
                    }
                }
            }
        }
    }
}