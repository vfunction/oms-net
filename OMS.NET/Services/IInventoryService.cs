﻿using OMS.NET.Models;

namespace OMS.NET.Services
{
    public interface IInventoryService
    {
        Inventory CreateInventory(Inventory inventory);
        Inventory FetchInventory(string skuId);
    }
}