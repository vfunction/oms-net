﻿using OMS.NET.Models;

namespace OMS.NET.Services
{
    public interface IShippingService
    {
        Shipping CreateShipping(Shipping shipping);
        Shipping FetchShippingCharges(string skuId);
    }
}