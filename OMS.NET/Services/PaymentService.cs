﻿using OMS.NET.Integrations;
using OMS.NET.Models;
using System;

namespace OMS.NET.Services
{
    internal class PaymentService : IPaymentService
    {
        IPaymentHttpClient paymentHttpClient = new PaymentHttpClient();

        public AuthorizationResponseDto Authorize(AuthorizationRequestDto authorizationRequestDto)
        {
            return paymentHttpClient.Authorize(authorizationRequestDto);
        }

        public AuthorizationResponseDto ReverseAuth(AuthorizationRequestDto authorizationRequestDto)
        {
            return paymentHttpClient.ReverseAuth(authorizationRequestDto);
        }
    }
}