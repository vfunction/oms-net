﻿using OMS.NET.Models;

namespace OMS.NET.Services
{
    internal interface IPaymentService
    {
        AuthorizationResponseDto Authorize(AuthorizationRequestDto authorizationRequestDto);
        AuthorizationResponseDto ReverseAuth(AuthorizationRequestDto authorizationRequestDto);
    }
}