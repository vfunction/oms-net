﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace OMS.NET.Services
{
    public class StoreSearchService : IStoreSearchService
    {
        private int useCnt = 0;

        public List<string> FetchStoresByZipCode(string zipCode)
        {
            List<string> storesList = new List<string>();
            storesList.Add("281");
            storesList.Add("282");
            storesList.Add("283");
            storesList.Add("284");
            storesList.Add(string.Format("{0}", Interlocked.Increment(ref useCnt)));
            return storesList;
        }
    }
}