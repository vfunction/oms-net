﻿using Newtonsoft.Json;
using OMS.NET.Models;
using System;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;

namespace OMS.NET.Services
{
    internal class InventoryService : IInventoryService
    {
        private OMSContext db = new OMSContext();

        public Inventory FetchInventory(string skuId)
        {
            return db.inventories.Find(skuId);
        }

        public Inventory CreateInventory(Inventory inventory)
        {
            db.inventories.AddOrUpdate(inventory);
            db.SaveChanges();

            var asJson = JsonConvert.SerializeObject(inventory);
            using (StreamWriter writetext = new StreamWriter("inventory.txt"))
            {
                writetext.WriteLine(asJson);
            }
            using (StreamReader readtext = new StreamReader("inventory.txt"))
            {
                string readText = readtext.ReadLine();
                Console.WriteLine(readText);
            }
            return db.inventories.First(inv => inv.SkuId == inventory.SkuId);
        }
    }
}