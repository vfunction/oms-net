﻿using OMS.NET.Integrations;
using OMS.NET.Models;
using System;

namespace OMS.NET.Services
{
    internal class EmailService : IEmailService
    {
        IEmailHttpClient emailServiceHttpClient = new EmailHttpClient();

        public string SendEmail(EmailRequestDto emailRequestDto)
        {
            return emailServiceHttpClient.SendEmail(emailRequestDto);
        }
    }
}