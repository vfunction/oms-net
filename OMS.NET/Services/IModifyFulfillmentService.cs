﻿using OMS.NET.Models;

namespace OMS.NET.Services
{
    public interface IModifyFulfillmentService
    {
        EmailRequestDto buildEmailRequest(SalesOrder salesOrder);
        SalesOrder ModifyToShipping(string lineItemId, SalesOrder salesOrder);
        SalesOrder ModifyToStorePickup(string lineItemId, SalesOrder salesOrder);
    }
}