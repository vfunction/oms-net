﻿using OMS.NET.Models;
using OMS.NET.Services;
using OMS.NET.Util;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;

namespace OMS.NET.Services
{
    internal class ProductService : IProductService
    {
        OMSContext db = new OMSContext();
        public virtual IInventoryService inventorySerice { get; set; }
        public virtual IShippingService shippingService { get; set; }
        Logger logger = new Logger();

        public Product GetProductById(string productId)
        {
            logger.Log(this.GetType().Name);
            return db.products.Find(productId);
        }

        public Inventory GetProductInventory(string productId)
        {
            logger.Log(this.GetType().Name);
            return inventorySerice.FetchInventory(productId);
        }

        public List<Inventory> GetInventoriesDescribedWith(string text)
        {
            logger.Log(this.GetType().Name);
            List<Inventory> inventoryList = new List<Inventory>();

            var products = db.products.SqlQuery("SELECT * from dbo.products WHERE description LIKE '%@p0%'", text).ToList();
            foreach (Product p in products)
            {
                inventoryList.Add(GetProductInventory(p.ProductId));
            }
            return inventoryList;

        }

        public List<OrderLine> GetOrderLinesForProduct(string productId)
        {
            logger.Log(this.GetType().Name);
            return (from o in db.orders where o.CustomerSKU == productId select o).ToList();
        }

        public double GetTotalChargesForProduct(string productId)
        {
            logger.Log(this.GetType().Name);
            double res = 0;

            List<OrderLine> orderLines = GetOrderLinesForProduct(productId);
            foreach (OrderLine orderLine in orderLines)
            {
                LineCharges charges = orderLine.LineCharges;
                res += charges.TotalCharges;
            }

            return res;
        }

        public List<Product> GetAllProducts()
        {
            logger.Log(this.GetType().Name);
            return db.products.ToList();
        }

        public Product GetProductByName(string productName)
        {
            logger.Log(this.GetType().Name);
            return (from p in db.products where p.Name == productName select p).FirstOrDefault();
        }

        public List<Product> GetProductsDescribedWith(string text)
        {
            logger.Log(this.GetType().Name);
            return (from p in db.products where p.Description.Contains(text) select p).ToList();
        }

        public Product RegisterProduct(Product product)
        {
            logger.Log(this.GetType().Name);
            db.products.AddOrUpdate(product);
            db.SaveChanges();
            return db.products.First(p => p.ProductId == product.ProductId);
        }
    }
}