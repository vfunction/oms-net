﻿using OMS.NET.Models;

namespace OMS.NET.Services
{
    internal interface IEmailService
    {
        string SendEmail(EmailRequestDto emailRequestDto);
    }
}