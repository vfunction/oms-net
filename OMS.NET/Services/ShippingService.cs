﻿using log4net.Repository.Hierarchy;
using OMS.NET.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace OMS.NET.Services
{
    
    interface IShippingWriter
    {
        void Write(string filepath, string skuId);
    }

    class ShippingWriter: IShippingWriter
    {
        void IShippingWriter.Write(string filepath, string skuId)
        {
            File.WriteAllText(filepath, $"SkuID is {skuId}");
        }
    }

    public class ShippingService : IShippingService
    {
        private OMSContext db = new OMSContext();

        private ShippingWriter writer = new ShippingWriter();

        public Shipping FetchShippingCharges(string skuId)
        {
            ManipulateShippingCharges(new Dictionary<string, List<int>> {
                {"name1", new List<int>{1, 2, 3, 4, 5} },
                {"name2", new List<int>{6, 7, 8, 9, 10} },


            }, extra: 10);
            var fp = WriteShippingCharges(skuId);
            File.Delete(fp);
            return db.shippings.Find(skuId);
        }

        public Shipping CreateShipping(Shipping shipping)
        {
            var res = db.shippings.Add(shipping);
            db.SaveChanges();
            ManipulateShippingCharges(new Dictionary<string, List<int>> {
                {"name1", new List<int>{1, 2, 3, 4, 5} },
                {"name2", new List<int>{6, 7, 8, 9, 10} },


            }, extra: 10);

            return res;
        }

        public void ManipulateShippingCharges(Dictionary<string, List<int>> charges, int extra)
        {
            var res = new Dictionary<string, List<int>>();
            charges.ToList().ForEach(chargeEntry =>
            {
                var vals = chargeEntry.Value.Select(x => x + extra);
                res[chargeEntry.Key] = vals.ToList();
            });

            charges = res;
        }

        public string WriteShippingCharges(string skuId)
        {
            var p = Path.GetTempFileName();
            var iWriter = writer as IShippingWriter;
            iWriter.Write(p, skuId);

            return p;
        }
    }
}