﻿using OMS.NET.Models;

namespace OMS.NET.Services
{
    public interface IOrderService
    {
        SalesOrder FetchOrder(string customerOrderId);
        SalesOrder SaveOrder(SalesOrder salesOrder);
    }
}