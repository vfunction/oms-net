﻿using OMS.NET.Models;
using System.Collections.Generic;

namespace OMS.NET.Services
{
    public interface IProductService
    {
        List<Product> GetAllProducts();
        List<Inventory> GetInventoriesDescribedWith(string text);
        List<OrderLine> GetOrderLinesForProduct(string productId);
        Product GetProductById(string productId);
        Product GetProductByName(string productName);
        Inventory GetProductInventory(string productId);
        List<Product> GetProductsDescribedWith(string text);
        double GetTotalChargesForProduct(string productId);
        Product RegisterProduct(Product product);
    }
}