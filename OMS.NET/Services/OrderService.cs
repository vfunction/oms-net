﻿using OMS.NET.Models;
using System;
using System.Data.Entity.Migrations;
using System.Linq;

namespace OMS.NET.Services
{
    internal class OrderService : IOrderService
    {
        private OMSContext db = new OMSContext();

        public SalesOrder FetchOrder(string customerOrderId)
        {
            return db.sales.Find(customerOrderId);
        }

        public SalesOrder SaveOrder(SalesOrder salesOrder)
        {
            var billingAddress = db.billingAddresses.Find(salesOrder.BillToAddress.BillToAddressId);
            if (billingAddress != null)
            {
                salesOrder.BillToAddressId = billingAddress.BillToAddressId;
                salesOrder.BillToAddress = null;
            }

            var charges = db.charges.Find(salesOrder.Charges.ChargesId);
            if (charges != null)
            {
                salesOrder.ChargesId = charges.ChargesId;
                salesOrder.Charges = null;
            }

            var payment = db.payments.Find(salesOrder.PaymentInfo.PaymentId);
            if (payment != null)
            {
                salesOrder.PaymentInfoId = payment.PaymentId;
                salesOrder.PaymentInfo = null;
            }

            foreach (var ol in salesOrder.OrderLines)
            {
                var shippingAddress = db.shippingAddresses.Find(ol.ShipToAddress.ShipToAddressId);
                if (shippingAddress != null)
                {
                    ol.ShipToAddressId = shippingAddress.ShipToAddressId;
                    ol.ShipToAddress = null;
                }

                var lineCharges = db.lineCharges.Find(ol.LineCharges.LineChargesId);
                if (lineCharges != null)
                {
                    ol.LineChargesId = lineCharges.LineChargesId;
                    ol.LineCharges = null;
                }
            }

            db.sales.AddOrUpdate(salesOrder);
            db.SaveChanges();
            return db.sales.First(s => s.CustomerOrderId == salesOrder.CustomerOrderId);

        }
    }
}