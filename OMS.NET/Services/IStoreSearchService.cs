﻿using System.Collections.Generic;

namespace OMS.NET.Services
{
    public interface IStoreSearchService
    {
        List<string> FetchStoresByZipCode(string zipCode);
    }
}