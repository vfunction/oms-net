﻿using Castle.MicroKernel;
using Castle.MicroKernel.Lifestyle;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using OMS.NET.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dependencies;
using System.Web.Mvc;
using System.Web.Routing;

namespace OMS.NET.Controllers
{

	internal class DependencyResolver : System.Web.Http.Dependencies.IDependencyResolver
	{
		private readonly IKernel kernel;

		public DependencyResolver(IKernel kernel)
		{
			this.kernel = kernel;
		}

		public IDependencyScope BeginScope()
		{
			return new DependencyScope(kernel);
		}

		public object GetService(Type type)
		{
			return kernel.HasComponent(type) ? kernel.Resolve(type) : null;
		}

		public IEnumerable<object> GetServices(Type type)
		{
			return kernel.ResolveAll(type).Cast<object>();
		}

		public void Dispose()
		{
		}
	}

	public class DependencyScope : IDependencyScope
	{
		private readonly IKernel kernel;

		private readonly IDisposable disposable;

		public DependencyScope(IKernel kernel)
		{
			this.kernel = kernel;
			disposable = kernel.BeginScope();
		}

		public object GetService(Type type)
		{
			return kernel.HasComponent(type) ? kernel.Resolve(type) : null;
		}

		public IEnumerable<object> GetServices(Type type)
		{
			return kernel.ResolveAll(type).Cast<object>();
		}

		public void Dispose()
		{
			disposable.Dispose();
		}
	}

	public class ControllersInstaller : IWindsorInstaller
	{
		public void Install(IWindsorContainer container, IConfigurationStore store)
		{
			container.Register(Classes.FromThisAssembly()
								   .BasedOn<ApiController>()
								   .LifestyleTransient());
		}
	}

	public class ServiceInstaller : IWindsorInstaller
	{	
		public void Install(IWindsorContainer container, IConfigurationStore store)
		{
			container.Register(Component.For<IInventoryService>().ImplementedBy<InventoryService>());
			container.Register(Component.For<IEmailService>().ImplementedBy<EmailService>());
			container.Register(Component.For<IModifyFulfillmentService>().ImplementedBy<ModifyFulfillmentService>());
			container.Register(Component.For<IOrderService>().ImplementedBy<OrderService>());
			container.Register(Component.For<IPaymentService>().ImplementedBy<PaymentService>());
			container.Register(Component.For<IProductService>().ImplementedBy<ProductService>());
			container.Register(Component.For<IShippingService>().ImplementedBy<ShippingService>());
			container.Register(Component.For<IStoreSearchService>().ImplementedBy<StoreSearchService>());
		}
	}
}
