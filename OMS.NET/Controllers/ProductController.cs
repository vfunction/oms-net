﻿using Newtonsoft.Json;
using OMS.NET.Models;
using OMS.NET.Services;
using OMS.NET.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OMS.NET.Controllers
{
    [RoutePrefix("api/product")]
    public class ProductController : ApiController
    {
        public virtual IProductService productService { get; set; }
        Logger logger = new Logger();

        [HttpGet]
        [Route("{productId}")]
        public Product getProductById([FromUri] string productId)
        {
            logger.Log(this.GetType().Name);
            return productService.GetProductById(productId);
        }

        [HttpGet]
        [Route("inv/{productId}")]
        public Inventory getInventoryForProduct([FromUri] string productId)
        {
            logger.Log(this.GetType().Name);
            return productService.GetProductInventory(productId);
        }

        [HttpGet]
        [Route("inv-desc/{text}")]
        public Inventory[] getInventoryForProductByDesc([FromUri] string text)
        {
            logger.Log(this.GetType().Name);
            return productService.GetInventoriesDescribedWith(text).ToArray();
        }


        [HttpGet]
        [Route("orderlines/{productId}")]
        public OrderLine[] getOrderLinesForProduct([FromUri] string productId)
        {
            logger.Log(this.GetType().Name);
            return productService.GetOrderLinesForProduct(productId).ToArray();
        }

        [HttpGet]
        [Route("charges/{productId}")]
        public Double getChargesForProduct([FromUri] string productId)
        {
            logger.Log(this.GetType().Name);
            return productService.GetTotalChargesForProduct(productId);
        }


        [HttpGet]
        [Route("all")]
        public Product[] getProductById()
        {
            logger.Log(this.GetType().Name);
            return productService.GetAllProducts().ToArray();
        }


        [HttpGet]
        [Route("name/{productName}")]
        public Product getProductByName([FromUri] string productName)
        {
            logger.Log(this.GetType().Name);
            return productService.GetProductByName(productName);
        }

        [HttpGet]
        [Route("desc-includes/{text}")]
        public Product[] getProductsDescribedBy([FromUri] string text)
        {
            logger.Log(this.GetType().Name);
            return productService.GetProductsDescribedWith(text).ToArray();
        }


        [HttpPost]
        [Route("register")]
        public Product registerNewProduct([FromBody] Product product)
        {
            logger.Log(this.GetType().Name);
            return productService.RegisterProduct(product);
        }

        [HttpPost]
        [Route("register-list")]
        public Product[] registerNewProducts([FromBody] Product[] productArray)
        {
            logger.Log($"Trying to register new products {JsonConvert.SerializeObject(productArray)}");
            try
            {
                Product[] registeredProducts = new Product[productArray.Length];
                int i = 0;
                logger.Log(this.GetType().Name);
                foreach (Product p in productArray)
                {
                    registeredProducts[i] = registerNewProduct(p);
                    i++;
                }
                return registeredProducts;
            }
            catch (Exception ex)
            {
                logger.Log($"Execption on registerNewProducts. {ex.GetBaseException().Message}: {ex.StackTrace}");
                throw ex;
            }

        }
    }
}