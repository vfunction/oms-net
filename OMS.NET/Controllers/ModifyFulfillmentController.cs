﻿using OMS.NET.Models;
using OMS.NET.Services;
using OMS.NET.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OMS.NET.Controllers
{
    [RoutePrefix("api/modify/fulfillment")]
    public class ModifyFulfillmentController : ApiController
    {
        public virtual IModifyFulfillmentService modifyFulfillmentService { get; set; }
        Logger logger = new Logger();

        [HttpPatch]
        [Route("shipping/items/{lineItemId}")]
        public SalesOrder modifyStorePickupToShipping([FromUri] string lineItemId, [FromBody] SalesOrder salesOrder)
        {
            logger.Log(this.GetType().Name);
            return modifyFulfillmentService.ModifyToShipping(lineItemId, salesOrder);
        }

        [HttpPatch]
        [Route("store/items/{lineItemId}")]
    public SalesOrder modifyShippingToStorePickUp([FromUri] string lineItemId, [FromBody] SalesOrder salesOrder)
        {
            logger.Log(this.GetType().Name);
            return modifyFulfillmentService.ModifyToStorePickup(lineItemId, salesOrder);
        }
    }
}