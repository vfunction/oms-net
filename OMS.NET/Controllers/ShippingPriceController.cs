﻿using OMS.NET.Models;
using OMS.NET.Services;
using OMS.NET.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OMS.NET.Controllers
{
    [RoutePrefix("api/shipping")]
    public class ShippingPriceController : ApiController
    {
        public virtual IShippingService shippingService { get; set; }
        Logger logger = new Logger();

        [HttpGet]
        [Route("{skuId}")]
        public Shipping fetchSalesOrder([FromUri] string skuId)
        {
            logger.Log(this.GetType().Name);
            return shippingService.FetchShippingCharges(skuId);
        }

        [HttpPost]
        [Route("")]
        public Shipping createShipping([FromBody] Shipping shipping)
        {
            logger.Log(this.GetType().Name);
            return shippingService.CreateShipping(shipping);
        }
    }
}