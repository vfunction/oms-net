﻿using Newtonsoft.Json;
using OMS.NET.Models;
using OMS.NET.Services;
using OMS.NET.Util;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OMS.NET.Controllers
{
    [RoutePrefix("api/order")]
    public class OrderController : ApiController
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public virtual IOrderService orderService { get; set; }
        Logger logger = new Logger();

        [HttpGet]
        [Route("{customerOrderId}")]
        public SalesOrder fetchSalesOrder([FromUri] string customerOrderId)
        {
            logger.Log(this.GetType().Name);
            return orderService.FetchOrder(customerOrderId);
        }

        [HttpPost]
        [Route("")]
        public SalesOrder createSalesOrder([FromBody] SalesOrder salesOrder)
        {
            logger.Log(this.GetType().Name);
            try
            {
                return orderService.SaveOrder(salesOrder);
            }
            catch (Exception ex)
            {
                log.Error(ex);
                throw ex;
            }
        }

        [HttpPost]
        [Route("multi")]
        public SalesOrder[] createMultipleOrders([FromBody] SalesOrder[] orderArray)
        {
            logger.Log($"Trying to create many orders. {JsonConvert.SerializeObject(orderArray)}");
            try
            {
                SalesOrder[] orders = new SalesOrder[orderArray.Length];
                int i = 0;
                foreach (SalesOrder order in orderArray)
                {
                    orders[i] = createSalesOrder(order);
                    i++;
                }
                return orders;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var v in e.EntityValidationErrors)
                {
                    foreach(var ve in v.ValidationErrors)
                    {
                        logger.Log($"Property: {ve.PropertyName}. Error: {ve.ErrorMessage}");
                    }
                }
                throw e;
            }
            catch (Exception e)
            {
                logger.Log($"Exception on createMultipleOrders. {e.GetBaseException().Message}\n{e.StackTrace}");
                throw e;
            }
        }
    }
}