﻿using OMS.NET.Services;
using OMS.NET.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace OMS.NET.Controllers
{
    [RoutePrefix("api/store")]
    public class StoreSearchController : ApiController
    {
        public virtual IStoreSearchService storeSearchService { get; set; }
        Logger logger = new Logger();

        [HttpGet]
        [Route("{zipCode}")]
        public async Task<List<string>> fetchStoresByZipAsync([FromUri] string zipCode)
        {
            logger.Log(this.GetType().Name);
            HttpClient client = new HttpClient();
            string responseBody = await client.GetStringAsync("https://vfunction.com/");
            Console.WriteLine(responseBody);

            WebClient webClient = new WebClient();
            var stream = webClient.OpenRead(new Uri("https://google.com/"));
            StreamReader reader = new StreamReader(stream);
            string pageText = reader.ReadToEnd();
            Console.WriteLine(pageText);
            
            return storeSearchService.FetchStoresByZipCode(zipCode);
        }
    }
}