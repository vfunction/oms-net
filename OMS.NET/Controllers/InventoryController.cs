﻿using Newtonsoft.Json;
using OMS.NET.Models;
using OMS.NET.Services;
using OMS.NET.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OMS.NET.Controllers
{
    [RoutePrefix("api/inventory")]
    public class InventoryController : ApiController
    {
        public virtual IInventoryService InventoryService { get; set; }
        Logger logger = new Logger();
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("{skuId}")]
        [HttpGet]
        public Inventory FetchInventory(string skuId)
        {
            logger.Log(this.GetType().Name);
            return InventoryService.FetchInventory(skuId);
        }

        [Route("")]
        [HttpPost]
        public Inventory CreateInventory([FromBody] Inventory inventory)
        {
            logger.Log($"Trying to create inventory: {JsonConvert.SerializeObject(inventory)}");
            try
            {
                return InventoryService.CreateInventory(inventory);

            }
            catch (Exception ex)
            {
                logger.Log($"Exception on CreateInventory.\n{ex.GetBaseException().Message}\n{ex.StackTrace}");
                throw ex;
            }
        }

        [Route("multi-create")]
        [HttpPost]
        public Inventory[] CreateMulti([FromBody] Inventory[] invArray)
        {
            Inventory[] createdInv = new Inventory[invArray.Length];
            int i = 0;
            logger.Log(this.GetType().Name);
            foreach (Inventory inv in invArray)
            {
                createdInv[i] = CreateInventory(inv);
                i++;
            }
            return createdInv;
        }
    }
}