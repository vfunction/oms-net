﻿using OMS.NET.Models;
using System;

namespace OMS.NET.Integrations
{
    internal interface IEmailHttpClient
    {
        string SendEmail(EmailRequestDto emailRequestDto);
    }
}