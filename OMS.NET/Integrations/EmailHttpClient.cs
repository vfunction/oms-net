﻿using OMS.NET.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace OMS.NET.Integrations
{
    public class EmailHttpClient : IEmailHttpClient
    {
        private int useCnt = 0;

        public string SendEmail(EmailRequestDto emailRequestDto)
        {
            return string.Format("SUCCESS {0}", Interlocked.Increment(ref useCnt));
        }
    }
}