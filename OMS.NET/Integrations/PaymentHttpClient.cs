﻿using OMS.NET.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace OMS.NET.Integrations
{
    public class PaymentHttpClient : IPaymentHttpClient
    {
        private int useCnt = 0;

        public AuthorizationResponseDto Authorize(AuthorizationRequestDto authorizationRequestDto)
        {
            return new AuthorizationResponseDto("123", 7.00, string.Format("SUCCESS {0}", Interlocked.Increment(ref useCnt)));
        }

        public AuthorizationResponseDto ReverseAuth(AuthorizationRequestDto authorizationRequestDto)
        {
            return new AuthorizationResponseDto("123", 7.00, string.Format("SUCCESS {0}", Interlocked.Increment(ref useCnt)));
        }
    }
}