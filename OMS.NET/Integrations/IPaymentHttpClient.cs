﻿using OMS.NET.Models;
using System;

namespace OMS.NET.Integrations
{
    internal interface IPaymentHttpClient
    {
        AuthorizationResponseDto Authorize(AuthorizationRequestDto authorizationRequestDto);
        AuthorizationResponseDto ReverseAuth(AuthorizationRequestDto authorizationRequestDto);
    }
}