﻿namespace OMS.NET.Models
{
    public class AuthorizationRequestDto
    {
        public AuthorizationRequestDto(string v1, string v2, string v3, string v4, double shippingAmount)
        {
            CardType = v1;
            CardNumber = v2;
            CardExpiryDate = v3;
            SecireCode = v4;
            ShippingAmount = shippingAmount;
        }
        public string CardType { get; }
        public string CardNumber { get; }
        public string CardExpiryDate { get; }
        public string SecireCode { get; }
        public double ShippingAmount { get; }
    }
}