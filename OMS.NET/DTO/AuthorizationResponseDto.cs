﻿namespace OMS.NET.Models
{
    public class AuthorizationResponseDto
    {
        public AuthorizationResponseDto(string v1, double v2, string v3)
        {
            Id = v1;
            Amount = v2;
            Status = v3;
        }

        public string Id { get; internal set; }
        public double Amount { get; internal set; }
        public string Status { get; }
    }
}