﻿namespace OMS.NET.Models
{
    public class EmailRequestDto
    {
        public EmailRequestDto(string v1, string v2, string v3, string v4)
        {
            SalesOrderNumber = v1;
            MessageTitle = v2;
            MessageBody = v3;
            EmailType = v4;
        }

        public string SalesOrderNumber { get; }
        public string MessageTitle { get; }
        public string MessageBody { get; }
        public string EmailType { get; }
    }
}