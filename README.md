# OMS .NET
## Setting Up Dev Env

### <a name="prereq"></a> Prerequisites

1. Install Visual Studio from [here](https://visualstudio.microsoft.com/vs/community), make sure to pick the following workloads:
    1. ASP.NET and web development, in the optional section, also check .Net Framework project and item templates
    1. .Net desktop development
    1. Data storage and processing
1. Install Sql Server Express from [here](https://go.microsoft.com/fwlink/?linkid=866658). Once installation is complete, DB is automatically created.

### <a name="build"></a> Building 

1. Clone this repository
1. Open OMS.NET.sln
1. Build the solution, if issues with .Nuget arise, you may need to add https://api.nuget.org/v3/index.json as a package source. If it still doesn't work see [here](https://docs.microsoft.com/en-us/nuget/Consume-Packages/package-restore#restore-packages-manually-using-visual-studio) for debug tips
1. Open Package Manager Console (Tools &rarr; NuGet Package Manager &rarr; Package Manager Console)
1. Run update-database

### Debugging from Windows (Postman)

1. From VS start debug session with IISExpress, a browser window should open up to the correct location.  
If you get and exception indicating it cannot find bin\roslyn, try running the following command from the Package Manager Console:  
`Update-Package Microsoft.CodeDom.Providers.DotNetCompilerPlatform -r`
1. Make API calls to that location followed by the controller route e.g. http://localhost:61234/api/order

### Deploying to IIS

There is an ansible script which installs IIS, SQL Server Express, creates the database and installs OMS.NET.
it can be found at https://bitbucket.org/vfunction/infra/src/master/ansible/playbooks/oms-net/

The instructions here are for your local machine, so that IIS serves the application off the working tree.

1. Complete all steps under [Prerequisites](#prereq) and [Building](#build)
1. In Web.config, change the connection string to the commented-out one (connect to SQLExpress instance instead of
   the developer-oriented LocalDB).
1. Make sure IIS is configured on your Windows (See instructions [here](https://superuser.com/questions/1218375/turn-windows-features-on-or-off-opens-server-manager) for Windows Server):
    1. Open the Windows Features window (Turn Windows features on or off)
    1. Expand "Internet Information Services"
    1. Check everything under "Web Management Tools" except "IIS 6 Management Compatibility"
    1. Under "World Wide Web Services"
        1. Check everything under "Application Development Features"
        1. Check everything under "Common HTTP Features" except "WebDav Publishing"
        1. Check everything under "Health and Diagnostics"
        1. Check everything under "Performance Features"
        1. Check "Request Filtering" under "Security"
1. Open IIS Manager.
1. Under "Application Pools" right-click DefaultAppPool &rarr; Advanced Settings...
1. Find "Identity" under "Process Model" and change the user from ApplicationPoolIdentity to the user you are signed in with
1. Right click "Sites &rarr; Default Web Site" and pick "Edit Bindings..."
1. Add a new binding with the following parameters
    1. Type: http
    1. IP Address: All Unassigned
    1. Port: 80
    1. Host name: dev.oms.net
1. Right clieck "Default Web Site" again and pick "Add Application..."
1. Fill in the following parameters:
    1. Alias: OMS.NET
    1. Application pool: DefaultAppPool
    1. Physical path: browse to the path of the OMS.NET project under the OMS.NET solution folder e.g. "C:\Users\OriSaporta\source\repos\OMS.NET\OMS.NET"
1. Click on the newley created application and in the "Features View" double-click "Connection Strings"
1. If one does not already exist, add a new connection string with the custom value "Server=localhost\SQLEXPRESS;Database=master;Trusted_Connection=True;" (without the quotation marks) 
1. Back in Windows Add dev.oms.net to your hosts for IP 127.0.0.1
1. Back in IIS manager, click on OMS.NET website and on the right bar, click "Browse dev.oms.net *:80 (http)"  
If you see a permissions issue, try [this](https://stackoverflow.com/questions/19162553/iis-401-3-unauthorized)
1. Make sure the browser opens up and you see the ASP.NET screen, you should also be able to click on API on the top bar and get the full API documentation

### Running against PGSQL Database

These instructions are for Windows dev machine:

1. Install [Postgres](https://www.postgresql.org/download/windows/)
   - During installation, use 'password' for the 'postgres' user, or adjust the password in Web.config connection string.
1. Using pgAdmin GUI application, create a new database named 'oms'
1. In Web.config, under <connectionStrings>, comment-out the existing connection string and uncomment the one
   with provider set to "Npgsql".
1. Rebuild the solution, and re-run update-database, as detailed in the [Building](#build) section.
   - The update-database command creates the schema in the 'oms' database

### Running the APIs script 

1. Install WSL and Linux Distro of your choosing
1. In Windows command line run ipconfig, look for "Ethernet adapter vEthernet (WSL):" and copy the "IPv4 Address" under it.
1. In WSL, edit your hosts file and add dev.oms.net for the IP from the previous step. These steps may require repeating after every restart becuase the WSL IP address changes.
1. To run the script, navigate in WSL to OMS.NET/script folder and run use-api-net.sh

## .Net Resources

https://www.entityframeworktutorial.net/code-first/configure-one-to-many-relationship-in-code-first.aspx

https://docs.microsoft.com/en-us/ef/ef6/modeling/code-first/data-annotations

https://docs.microsoft.com/en-us/aspnet/web-api/overview/data/using-web-api-with-entity-framework/part-3