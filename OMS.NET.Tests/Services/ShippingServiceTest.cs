﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OMS.NET.Services;
using System.IO;

namespace OMS.NET.Tests.Services
{
    [TestClass]
    public class ShippingServiceTest
    {
        [TestMethod]
        public void TestWriting()
        {
            ShippingService service = new ShippingService();
            var path = service.WriteShippingCharges("skuID");

            var content = File.ReadAllText(path);
            Assert.IsTrue(content.Count() > 0);
            File.Delete(path);
        }
    }
}
