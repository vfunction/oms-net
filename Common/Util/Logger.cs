﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Configuration;


namespace OMS.NET.Util
{
    public class Logger
    {
        private String path;

        public Logger()
        {
            var logPath = ConfigurationManager.AppSettings["LoggerFileLocation"];
            if (logPath != null)
            {
                SetPath(logPath);
            }
        }

        public void SetPath(String path)
        {
            this.path = path;
        }

        public void Log(String msg)
        {
            if (path == null)
            {
                //tests
                return;
            }
            using (StreamWriter sw = File.AppendText(path))
            {
                sw.WriteLine(msg);
            };
        }
    }
}