﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMS.NET.Models
{
    public class LineCharges
    {
        public string LineChargesId { get; set; }
        public double TotalCharges { get; set; }
    }
}