﻿using System.ComponentModel.DataAnnotations;

namespace OMS.NET.Models
{
    public class PaymentInfo
    {
        [Key]
        public string PaymentId { get; set; }
        public string PaymentStatus { get; set; }
        public string CardType { get; set; }
        public double AuthorizedAmount { get; set; }
        public double CollectedAmount { get; set; }
    }
}