﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OMS.NET.Models
{
    public class OrderLine
    {
        [Key]
        public string LineItemId { get; set; }
        public string ShipNode { get; set; }
        public string ShipNodeDescription { get; set; }
        public string LevelOfService { get; set; }
        public string PrimeLineNumber { get; set; }
        public string SubLineNumber { get; set; }
        public string CustomerSKU { get; set; }
        public string SkuDescription { get; set; }
        public string EstimatedArrivalDate { get; set; }
        public string Status { get; set; }
        public string ReshippedBefore { get; set; }

        [ForeignKey("ShipToAddress")]
        public string ShipToAddressId { get; set; }
        public ShipToAddress ShipToAddress { get; set; }

        [ForeignKey("LineCharges")]
        public string LineChargesId { get; set; }
        public LineCharges LineCharges { get; set; }
    }
}