﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OMS.NET.Models
{
    public class Inventory
    {
        [Key]
        public string SkuId { get; set; }
        public string StoreId { get; set; }
        public int Quantity { get; set; }

    }
}