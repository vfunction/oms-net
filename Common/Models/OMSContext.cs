﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace OMS.NET.Models
{
    public class OMSContext : DbContext
    {
        public OMSContext() : base("SqlExpress") { }

        public DbSet<Inventory> inventories { get; set; }
        public DbSet<OrderLine> orders { get; set; }
        public DbSet<BillToAddress> billingAddresses { get; set; }
        public DbSet<Charges> charges { get; set; }
        public DbSet<ShipToAddress> shippingAddresses { get; set; }
        public DbSet<LineCharges> lineCharges { get; set; }
        public DbSet<PaymentInfo> payments { get; set; }
        public DbSet<SalesOrder> sales { get; set; }
        public DbSet<Shipping> shippings { get; set; }
        public DbSet<Product> products { get; set; }
    }
}