﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OMS.NET.Models
{
    public class Shipping
    {
        [Key]
        public string SkuId { get; set; }
        public double StandardShipping { get; set; }
        public double ExpeditedShipping { get; set; }
        public double ExpressShipping { get; set; }
    }
}