﻿namespace OMS.NET.Models
{
    public class Charges
    {
        public string ChargesId { get; set; }
        public double LineSubTotal { get; set; }
        public double TotalCharges { get; set; }
        public double SalesTax { get; set; }
        public double GrandTotal { get; set; }
    }
}