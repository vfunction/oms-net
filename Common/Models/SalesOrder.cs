﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OMS.NET.Models
{
    public class SalesOrder
    {
        [Key]
        public string CustomerOrderId { get; set; }
        public string PrimaryPhone { get; set; }
        public string CustomerEmailId { get; set; }
        public string OrderStatus { get; set; }
        public string FirstName { get; set; }
        public string OrderDate { get; set; }
        public string ProfileId { get; set; }
        public string LastName { get; set; }
        public string EntryType { get; set; }

        [ForeignKey("BillToAddress")]
        public string BillToAddressId { get; set; }
        public BillToAddress BillToAddress { get; set; }

        [ForeignKey("PaymentInfo")]
        public string PaymentInfoId { get; set; }
        public PaymentInfo PaymentInfo { get; set; }

        [ForeignKey("Charges")]
        public string ChargesId { get; set; }
        public Charges Charges { get; set; }

        public ICollection<OrderLine> OrderLines { get; set; }

    }
}